# python-test

## Goal

This python-test docker includes the test suites running python tests

It includes 2 tests:

* python-test1: echo 'Hello test1'
* python-test2: echo 'Hello test2'

## Usage

### Configuration

Mandatory:

* Not applicable

Optional:

* The local result directory path: to store the results in your local
  environement. It shall corresponds to the internal result docker path
  /var/lib/xtesting/results

### Command

You can run this docker by typing:

```shell
docker run -v <result directory>:/var/lib/xtesting/results \
registry.forge.orange-labs.fr/<your cuid/login>/xtesting_handson/python_test:latest
```

Options:

* -r: by default the reporting to the Database is not enabled. You need to
  specify the -r option in the command line. Please note that in this case, you
  must precise some env variables.

environement variables:

* Mandatory:
  * TEST_DB_URL: the url of the target Database with the env variable .
  * NODE_NAME: the name of your test environement. It must be declared in the
    test database (e.g. windriver-SB00)
* Optionnal
  * INSTALLER_TYPE: precise your VNF name and release
  * BUILD_TAG: a unique tag of your CI system. It can be usefull to get all the
    tests of one CI run. It uses the regex (dai|week)ly-(.+?)-[0-9]* to find the
    version (e.g. daily-elalto-123456789).

The command becomes:

```shell
docker run -v <result directory>:/var/lib/xtesting/results \
registry.forge.orange-labs.fr/<your cuid/login>/\
xtesting_handson/python_test:latest \
/bin/bash -c "run_tests -r -t all
```

### Output

```md
+--------------+-------------+-------------------+----------+--------+
| TEST CASE    | PROJECT     | TIER              | DURATION | RESULT |
+--------------+-------------+-------------------+----------+--------+
| python-test1 | python      | python_test       | 00:00    | PASS   |
| python-test2 | python      | python_test       | 00:00    | PASS   |
+--------------+-------------+-------------------+----------+--------+
```
